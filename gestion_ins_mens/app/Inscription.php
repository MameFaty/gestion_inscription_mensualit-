<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inscription extends Model
{
    protected $fillable = array ('classes_id','nom','prenom','date_naissance','lieu_naissance','tuteur','telephone_tuteur','email_tuteur');
    public static $rules = array('classes_id'=>'required|integer',
                                'nom'=>'required|min:5',
                                'prenom'=>'required|min:5',
                                'date_naissance'=>'required|min:5',
                                'lieu_naissance'=>'required|min:5',
                                'tuteur'=>'required|min:5',
                                'telephone_tuteur'=>'required|min:5',
                                'email_tuteur'=>'required|min:5',
                                );
    public function inscription()
    {
        return $this->belongsTo('App\Classe');
    }
}
