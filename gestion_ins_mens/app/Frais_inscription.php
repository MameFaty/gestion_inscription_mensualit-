<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Frais_inscription extends Model
{
    protected $fillable = array ('classes_id','inscriptions_id','date_inscription','mode_inscription','montant');
    public static $rules = array('classes_id'=>'required|integer',
                                'inscriptions_id'=>'required|integer',
                                'date_inscription'=>'required|min:5',
                                'mode_inscription'=>'required|min:5',
                                'montant'=>'required|min:5',
                                 );
    public function frais_inscription()
    {
                                     
        return $this->belongsTo('App\Classe');
        return $this->belongsTo('App\Inscription');
    }
}
