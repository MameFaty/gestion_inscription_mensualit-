<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Classe extends Model
{
    protected $fillable = array ('niveau','serie','classe','code_classe');
    public static $rules = array('niveau'=>'required|min:2',
                                'serie'=>'required|min:5',
                                'classe'=>'required|min:5',
                                'code_classe'=>'required|min:5',
                                );
    public function inscription()
    {
        return $this->hasMany('App\Inscription');
    }
    public function frais_inscription()
    {
        return $this->hasMany('App\Frais_inscription');
    }
    public function frais_mensualité()
    {
        return $this->hasMany('App\Frais_mensualité');
    }

}
