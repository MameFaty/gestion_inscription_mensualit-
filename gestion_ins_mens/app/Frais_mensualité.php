<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Frais_mensualité extends Model
{
    protected $fillable = array ('classes_id','inscriptions_id','mois');
    public static $rules = array('classes_id'=>'required|integer',
                                'inscriptions_id'=>'required|integer',
                                'mois'=>'required|min:5',
                                 );
    public function frais_mensualité()
    {
       return $this->belongsTo('App\Classe');
        return $this->belongsTo('App\Inscription');
    }
}
