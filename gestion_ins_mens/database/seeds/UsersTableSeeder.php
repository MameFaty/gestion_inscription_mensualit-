<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

        $adminRole = Role::where('nom', 'ADMIN')->first();
        $secretaireRole = Role::where('nom', 'SECRETAIRE')->first();
        $parent1Role = Role::where('nom', 'PARENT1')->first();
        $parent2Role = Role::where('nom', 'PARENT2')->first();

        $admin = User::create([
            'nom' => 'Diouf ',
            'prenom' => 'Macodou ',
            'email' => 'mac.koonda@admin.com',
            'password' => bcrypt('Dadimak2012$')
        ]);

        $secretaire = User::create([
            'nom' => 'Amy',
            'prenom' => 'Niane',
            'email' => 'amy@secretaire.com',
            'password' => bcrypt('Amy2012$')
        ]);

        $parent1 = User::create([
            'nom' => 'Mamadou',
            'prenom' => 'NDIAYE',
            'email' => 'modou@parent.com',
            'password' => bcrypt('mamadou2012$')
        ]);

        $parent2 = User::create([
            'nom' => 'Fatou',
            'prenom' => 'DIOP',
            'email' => 'fatou@parent.com',
            'password' => bcrypt('Fatou2012$')
        ]);

        $admin->roles()->attach($adminRole);
        $secretaire->roles()->attach($secretaireRole);
        $parent1->roles()->attach($parent1Role);
        $parent2->roles()->attach($parent2Role);
    }
}
