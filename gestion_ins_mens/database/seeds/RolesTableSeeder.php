<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::truncate();
        Role::create(['nom' => 'ADMIN']);
        Role::create(['nom' => 'SECRETAIRE']);
        Role::create(['nom' => 'PARENT1']);
        Role::create(['nom' => 'PARENT2']);
    }
}
