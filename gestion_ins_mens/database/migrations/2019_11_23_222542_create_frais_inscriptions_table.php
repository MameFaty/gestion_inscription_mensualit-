<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFraisInscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('frais_inscriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('classes_id')->unsigned();
            $table->foreign('classes_id')->references('id')->on('classes');
            $table->integer('inscriptions_id')->unsigned();
            $table->foreign('inscriptions_id')->references('id')->on('inscriptions');
            $table->dateTime('date_inscription');
            $table->string('mode_inscription');
            $table->string('montant');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('frais_inscriptions');
    }
}
